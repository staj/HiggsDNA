#pragma once
/**
 * Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * SPDX-License-Identifier: Apache-2.0.
 */

#define AWS_CRT_CPP_VERSION "0.27.3-dev+abc59fad"
#define AWS_CRT_CPP_VERSION_MAJOR 0
#define AWS_CRT_CPP_VERSION_MINOR 27
#define AWS_CRT_CPP_VERSION_PATCH 3
#define AWS_CRT_CPP_GIT_HASH "abc59fad8a5e3ce8f50e4add75fa8f7ef36dfd04"
