import awkward as ak
import numpy as np
import vector

vector.register_awkward()


def get_Hgg(
    self, diphotons: ak.highlevel.Array, dijets: ak.highlevel.Array
) -> ak.highlevel.Array:
    # Script adapted from the Zmumug analysis
    # combine dijet & diphoton
    dijets["charge"] = ak.zeros_like(dijets.pt, dtype=np.int)
    Hgg_jagged = ak.cartesian({"diphoton": diphotons, "dijet": dijets}, axis=1)
    # flatten Hgg, selection only accept flatten arrays
    count = ak.num(Hgg_jagged)
    Hgg = ak.flatten(Hgg_jagged)

    # diphoton and dijet obj
    diphoton_obj = Hgg.diphoton.pho_lead + Hgg.diphoton.pho_sublead
    dijet_obj = Hgg.dijet.first_jet + Hgg.dijet.second_jet

    Hgg_obj = Hgg.diphoton.pho_lead + Hgg.diphoton.pho_sublead + Hgg.dijet.first_jet + Hgg.dijet.second_jet

    # dress other variables
    Hgg["obj_diphoton"] = diphoton_obj
    Hgg["pho_lead"] = Hgg.diphoton.pho_lead
    Hgg["pho_sublead"] = Hgg.diphoton.pho_sublead
    Hgg["obj_dijet"] = dijet_obj
    Hgg["first_jet"] = Hgg.dijet.first_jet
    Hgg["second_jet"] = Hgg.dijet.second_jet
    Hgg["obj_Hgg"] = Hgg_obj
    Hgg_jagged = ak.unflatten(Hgg, count)

    # get best matched Hgg for each event
    best_Hgg = ak.firsts(Hgg_jagged)

    return best_Hgg

def DeltaR(photon, jet):
    pho_obj = ak.with_name(photon,"Momentum4D")
    jet_obj = ak.with_name(jet,"Momentum4D")
    return vector.Spatial.deltaR(pho_obj,jet_obj)


def Cxx(higgs_eta, VBFjet_eta_diff, VBFjet_eta_sum):
    # Centrality variable
    return np.exp(-4 / (VBFjet_eta_diff)**2 * (higgs_eta - (VBFjet_eta_sum) / 2)**2)
