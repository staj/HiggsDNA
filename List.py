
import os

def list_root_files(directory):
    root_files = []
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith('.root'):
                root_files.append(os.path.join(root, file))
    return root_files

# Example usage:
directory_path = "/eos/user/s/staj/HiggsDNA/vbf_EE_M115"
root_files_list = list_root_files(directory_path)

# Print the list of root files with their full paths
for root_file in root_files_list:
    print(root_file)

