
# Example command: python parquet_converter.py -i dy.parquet -o dy.root

import argparse
import uproot as up
import awkward as ak
import numpy as np


parser = argparse.ArgumentParser(description='manual to this script')
parser.add_argument("-i", "--input", help="input file",  default=None)
parser.add_argument("-o", "--output", help="output file",  default=None)
args = parser.parse_args()

if __name__ == "__main__":
    if args.input==None or args.output==None:
        print("[parquet converter] Please give the input and output filenames!!!")
        exit(0)

    print("[parquet converter] >>> Start")
    print("[parquet converter] Input:",args.input)
    print("[parquet converter] Output:",args.output)
    eve = ak.from_parquet(args.input)
    ntuple = {}
    for i in eve.fields:
        ntuple[i] = ak.Array(ak.to_list(eve[i]))
    with up.recreate(args.output) as fout:
        fout["Events"] = ntuple

    # eve_rdf = ak.to_rdataframe(ntuple)
    # eve_rdf.Snapshot("Events",args.output)
    print("[parquet converter] >>> Done")


