# HiggsDNA  
HiggsDNA (Higgs to Diphoton NanoAOD Framework) is the tool used for Higgs to diphoton related analyses in Run3. The updated and maintained documentation is available [here](https://higgs-dna.readthedocs.io/en/latest/index.html#).

The sample of json files can be found in this link and also the examples.

Then for lowmass analysis we have lowmass processor at [https://gitlab.cern.ch/staj/HiggsDNA/-/blob/master/higgs_dna/workflows/lowmass.py] 

Follow the instructions in HiggsDNA and one can find runner.json and samples.json files at [https://gitlab.cern.ch/staj/HiggsDNA/-/blob/master/higgs_dna]

if you are working on the lxplus then it's easy to deal with sample files because we will not. be in need of transferring the files. Just follow the instructions as mentioned here 

Make a text file where you just need to put the datafile name for all samples you need for example for vbf samples [https://gitlab.cern.ch/staj/HiggsDNA/-/blob/master/higgs_dna/samples_M_60.txt]

Then run the command 
fetch.py --input 70GeV_sample.txt -w Eurasia
It will create a json file and you can just use it late 